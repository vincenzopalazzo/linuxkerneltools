#!/usr/bin/env bash

echo 'creating image at $1`'
cd $1
wget https://raw.githubusercontent.com/google/syzkaller/master/tools/create-image.sh -O create-image.sh
chmod +x create-image.sh
./create-image.sh
echo 'image is ready!'

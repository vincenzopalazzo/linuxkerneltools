#!/usr/bin/env bash

# an example of usage can be `ARCH=qemu-system-x86_64 bash run-qemu.sh <path kernel> <path os imge create with the dev script create-os-image.sh>`

echo "Building on arch=$ARCH"
$ARCH -m 2G \
        -smp 2 \
        -kernel $1 \
        -append "console=ttyS0 root=/dev/sda earlyprintk=serial net.ifnames=0 nokaslr" \
        -drive file=$2,format=raw \
        -net user,host=10.0.2.10,hostfwd=tcp:127.0.0.1:10021-:22 \
        -net nic,model=e1000 \
        -enable-kvm \
        -nographic \
        -pidfile vm.pid \
        2>&1 | tee vm.log
